import axios from 'axios';

const githubRequest = axios.create({
    baseURL: 'https://api.github.com'
});
let queryString = ''

export const perPageCount = 10;

export const searchRepoApi = data => {
    queryString = data.query || '';
    return githubRequest.get(`/search/repositories?q=${data.query}&page=${data.page}&per_page=${perPageCount}`);
}
export const fetchMoreRepoApi = page => githubRequest.get(`/search/repositories?q=${queryString}&page=${page}&per_page=${perPageCount}`);

