import React from 'react';
import 'intersection-observer'
import '../public/css/App.css';
import SearchSection from '../components/search';

function App() {
  return (
    <div className="App">
      <SearchSection />
    </div>
  );
}

export default App;
