import React, { Component, Fragment } from 'react';
import InputControl from '../common/InputControl';
import ResultList from "./ResultList";
import { perPageCount, searchRepoApi, fetchMoreRepoApi} from '../../api';
import { isEmpty, get, debounce } from 'lodash';

class SearchSection extends Component{
  constructor(props) {
      super(props);
      this.state = {
          hasMore: true,
          totalPage: 0,
          page: 0,
          fetchErrCount: 0,
          isSearchNothing: false,
          results: [],
      };
      this.changeCallback = this.changeCallback.bind(this);
      this.fetchMore = this.fetchMore.bind(this);
  }

  changeCallback(val) {
    this.setState({
      page: 0,
      results: []
    })
    if(isEmpty(val)) return this.setState({ isSearchNothing: false })
    return searchRepoApi({
        query: val,
        page: '1'
    })
    .then(res => {
      window.scrollTo(0, 0);
      const results = get(res, 'data.items', [])
      const total_count = get(res, 'data.total_count', 0)
      if(isEmpty(results)) return this.setState({ isSearchNothing: true })
      return this.setState({
        page: 1,
        hasMore: total_count > perPageCount,
        results,
        totalPage: Math.ceil(total_count / perPageCount),
        fetchErrCount: 0,
        isSearchNothing: false
      })
    })
    .catch(err => {
      console.log('SearchRepoErr:::', err.data);
    })
  }

  fetchMore() {
    const nextPage = this.state.page +1;
    if(nextPage > this.state.totalPage) return null;
    return fetchMoreRepoApi(nextPage)
    .then(res => {
      const moreResults = get(res, 'data.items', [])
      this.setState({
        page: nextPage,
        results: this.state.results.concat(moreResults),
        hasMore: !(nextPage === this.state.totalPage)
      })
    })
    .catch(err => {
      console.log('FetchMoreErr:::', err);
      if(this.state.fetchErrCount === 3) return this.setState({ hasMore: false })
      return this.setState({
        page: nextPage,
        hasMore: !(nextPage === this.state.totalPage),
        fetchErrCount: this.state.fetchErrCount + 1,
      })
      /**
       * fetch 第Ｎ頁 error 時, 為不影響使用者瀏覽,
       * 仍setState, 以利後續頁面fetch。
       * 若連續三頁無fetch 失敗 -> 顯示已搜完
       */
    })
  }

  render() {
    const { results, hasMore, isSearchNothing } = this.state;
    return (
      <Fragment>
        <div className="App-search">
            <InputControl label='GitHub Repo 🔍' changeCallback={debounce(this.changeCallback, 1000)}/>
        </div>
        <div className="App-list">
            {isSearchNothing && <p className='App-txt'>Can't find nothing, please research...</p>}
            <ResultList fetchMore={this.fetchMore} results={results} hasMore={hasMore}/>
        </div>
      </Fragment>
    );
  }
  
}

export default SearchSection;
