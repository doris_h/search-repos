import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import ResultItem from './ResultItem'
import Loading from '../common/Loading';
import InfiniteScroll from '../common/InfiniteScroll'

function  ResultList ({ results, hasMore, fetchMore }){
   if(isEmpty(results)) return null;
   const resultItem = results.map((item, idx) => <ResultItem key={item.id+idx} item={item}/>);
   return (
      <InfiniteScroll
         loadMore={fetchMore}
         hasMore={hasMore}
         loader={Loading}
         loadEnd={<p className='App-txt'>----- The End -----</p>}
      >
      {resultItem}
      </InfiniteScroll>
   );
}

ResultList.defaultProps = {
   hasMore: false,
   results: [],
   fetchMore: () => {},
};
 
ResultList.propTypes = {
   hasMore: PropTypes.bool,
   results: PropTypes.array,
   fetchMore: PropTypes.func,
};

export default ResultList;
