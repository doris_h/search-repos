/* eslint-disable jsx-a11y/anchor-has-content */
import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import { get } from 'lodash';
import star from '../../public/svg/star.svg';
import fork from '../../public/svg/fork.svg';
import watch from '../../public/svg/watch.svg';

function ResultItem({ item }) {
  const imgStyle = {
      backgroundImage: `url(${get(item, 'owner.avatar_url', 'https://upload.wikimedia.org/wikipedia/commons/b/b3/Solid_gray.png')}),
      url(https://upload.wikimedia.org/wikipedia/commons/b/b3/Solid_gray.png)`,
  };
  return (
    <Container fluid className="App-item" >
      <Row>
        <Col sm={2}>
            <a role="button" className="App-avatar" href={get(item, 'html_url', '#')} style={imgStyle} />
        </Col>
        <Col sm={10}>
            <h4>
                <a role="button" tabIndex="0" href={get(item, 'html_url', '#')}>{get(item, 'full_name', '')}</a>
            </h4>
            <p>{get(item, 'description', '')}</p>
            <div>
              <div>
                <img src={star} className="App-icon" alt="icon"/> 
                <span>{get(item, 'stargazers_count', 0)}</span>
                <img src={fork} className="App-icon" alt="icon"/> 
                <span>{get(item, 'forks_count', 0)}</span>
                <img src={watch} className="App-icon" alt="icon"/> 
                <span>{get(item, 'watchers_count', 0)}</span>
              </div>
              <span>Updated at {get(item, 'updated_at', '')} </span>
            </div>
        </Col>
      </Row>
    </Container>
  );
}

ResultItem.defaultProps = {
  item: {},
};

ResultItem.propTypes = {
  item: PropTypes.object,
}

export default ResultItem;
