import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputVal: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    handleInputChange (event) {
        this.setState(
            { inputVal: event.target.value },
            () => this.props.changeCallback(this.state.inputVal)
        )
    }
    render() {
        return (
            <div>
                <h3>{this.props.label}</h3>
                <input 
                    value={this.state.inputVal}
                    onChange={this.handleInputChange}
                />
            </div>
        );
    }
}

InputControl.defaultProps = {
    label: '',
    changeCallback: () => {},
};
  
InputControl.propTypes = {
    label: PropTypes.string,
    changeCallback: PropTypes.func,
};
  
export default InputControl;
