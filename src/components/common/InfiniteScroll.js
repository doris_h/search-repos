import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

class InfiniteScroll extends React.Component {
    constructor(props) {
    super(props);
    this.loadingRef = React.createRef();
    this.state = {
        prevY: 0
    };
    this.handleObserver = this.handleObserver.bind(this);
    }
    componentDidMount() {
        var options = {
            root: null,
            rootMargin: "0px",
            threshold: 0.3
        };
        this.observer = new IntersectionObserver(
            this.handleObserver,
            options
        );
        if(this.loadingRef instanceof HTMLElement) this.observer.observe(this.loadingRef);
    }
  
    handleObserver (entities, observer) {
        const y = entities[0].boundingClientRect.y;
        if (this.state.prevY > y) this.props.loadMore();
        this.setState({ prevY: y });
    }
    render() {
        const { children, hasMore, loader: Loader, loadEnd } = this.props;
        return (
            <Fragment>
                {children}
                {hasMore ? <Loader loadingRef={el => this.loadingRef = el} /> : loadEnd}
            </Fragment>
        )
    }
}

InfiniteScroll.defaultProps = {
    hasMore: false,
    loader: () => {},
    loadMore: () => {},
    loadEnd: <Fragment/>
};

InfiniteScroll.propTypes = {
    hasMore: PropTypes.bool,
    loader: PropTypes.func,
    loadEnd: PropTypes.node,
    loadMore: PropTypes.func.isRequired,
}

export default InfiniteScroll;
