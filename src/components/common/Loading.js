import React from 'react';
import PropTypes from 'prop-types';
import loading from '../../public/svg/loading.svg';

function Loading ({ loadingRef }){

  return (
    <div className="App-loading" ref={loadingRef} >
      <img src={loading} alt="loading" width="57px"/>
    </div>
  );

}
Loading.defaultProps = {
  loadingRef: () => {},
};

Loading.propTypes = {
  loadingRef: PropTypes.func,
};
export default Loading;
