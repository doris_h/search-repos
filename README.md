# Search Github Repos

About searching repository on GitHub
[https://doris_h.gitlab.io/search-repos/index.html](https://doris_h.gitlab.io/search-repos/index.html)


## Folder Structure
架構以未來可擴充開發做設計
* `src/components/common` 模組化元件
* `src/components/common` 搜尋頁元件
* `src/public` CSS/Image..資源
* `src/api.js` API 統一於此設定

## Local Run Script

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!


